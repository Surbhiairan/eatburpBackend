var restaurantSchema = require('../app/models/restaurantSchema');
var dishSchema = require('../app/models/dishSchema');
var reviewSchema = require('../app/models/reviewSchema');
var dishRestaurantMappingSchema = require('../app/models/dishRestaurantMappingSchema');
var employee = require('../app/models/employeeSchema');
var userSchema = require('../app/models/users');

var bodyParser = require('body-parser');
var Promise = require('promise');
var express = require('express');
const jwt = require('jsonwebtoken');
var ObjectId = require('mongoose').Types.ObjectId;
var fs = require('fs');
var app = express();

app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
    extended: true
}));


var fs = require('fs');
var multer = require('multer');

var winston = require('winston');

var logger = new winston.Logger({
    level: 'error',
    transports: [
        new (winston.transports.File)({ filename: 'error.log' })
    ]
});

module.exports = function (app, AWS) {

    app.get('/', function (req, res) {
        console.log("jhihhi");
        res.render('index.ejs');
        //res.sendFile(__dirname + '/addResto.html');
    });

    app.get('/addResto', function (req, res) {
        console.log("jhihhi");
        res.render('addResto.ejs');
        //res.sendFile(__dirname + '/addResto.html');
    });

    app.post("/addResto", (req, res) => {
        var restaurantSchemaData = new restaurantSchema(req.body);
        console.log("restosData------------", restaurantSchemaData);
        restaurantSchemaData.save(function (err) {
            if (err) throw err;
        })
            .then(item => {
                res.send("Name saved to database");
            })
            .catch(err => {
                res.status(400).send("Unable to save to database");
            });
    });

    var s3 = new AWS.S3();

    var async = require('async');

    var upload = multer().array('photo', 25);
    const S3_BUCKET = process.env.S3_BUCKET;


    //var type = upload.array('photo', 25);

    app.post('/uploadrestoimage', function (req, res) {
        console.log("req----------", req.files);
        console.log(req.body);
        upload(req, res, function multerUpload(err) {
            //console.log("req inside upload------",req)
            console.log('building', req.body['address.building']);
            var userId;
            var token = req.headers['x-access-token'];
            if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

            jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
                if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
                else {
                    console.log('decoded-----------', decoded);
                    userId = decoded._id
                }
            });
            restaurantSchema.find(
                {
                    $and: [
                        { 'restaurant_name': req.body.name },
                        { 'address.building': req.body['address.building'] }
                    ]
                },
                (error) => {
                    if (error) {
                        console.log(error)
                    }
                })
                .then(result => {
                    console.log('result------', result)
                    if (result.length !== 0) {
                        console.log('already a entry in database---------');
                        res.status(400).json({ "message": "Please enter a new restaurant. this one already exsits" })
                        // do nothing
                    }
                    else {
                        console.log("now insert image along with data");
                        if (req) {
                            multipleFile(req).then(element => {
                                console.log("body-----------", req.body);
                                console.log("element--------", element);
                                req.body.images = element;
                                console.log("req.body-----------", req.body)
                                var restaurantSchemaData = new restaurantSchema(req.body);
                                //console.log("restosData------------", restosData);
                                restaurantSchemaData.save((err, result) => {
                                    if (err) {
                                        console.log("error--------", err);
                                        return res.status(400).json({ "message": err });
                                    } else {
                                        console.log("item details", result);
                                        //res.status(200).json({ "message": "Successfully saved" });
                                        employee.findOneAndUpdate(
                                            { _id: userId },
                                            {
                                                $push: {
                                                    added_restaurants_id: result._id
                                                }
                                            },
                                            function (err, result) {
                                                if (err) {
                                                    console.log(err);
                                                    return res.status(400).json({ "message": err });
                                                } else {
                                                    console.log(result, 'resultsssssssss')
                                                    return res.status(200).json({ "message": "Successfully saved" });
                                                }
                                            }
                                        )
                                    }
                                })
                                //res.send("uploaded successfully")
                            }).catch(err => {
                                console.log("error---------", err)
                                res.status(500).json({ "message": "error in saving data please report to technical team" + err })
                            })
                        } else {
                            console.log("inside else--------")
                            restaurantSchemaData.save((err, result) => {
                                if (err) {
                                    console.log("error--------", err);
                                    return res.status(400).json({ "message": err });
                                } else {
                                    console.log("item details", result);
                                    //res.status(200).json({ "message": "Successfully saved" });
                                    employee.findOneAndUpdate(
                                        { _id: userId },
                                        {
                                            $push: {
                                                added_restaurants_id: result._id
                                            }
                                        },
                                        function (err, result) {
                                            if (err) {
                                                console.log(err);
                                                res.status(400).json({ "message": err });
                                            } else {
                                                console.log(result, 'resultsssssssss')
                                                res.status(200).json({ "message": "Successfully saved" });
                                            }
                                        }
                                    )
                                }
                            })
                        }
                    }
                }
                )
        })
    });

    app.post('/addDish', function (req, res) {
        console.log("req----------", req.files);
        console.log(req.body);
        upload(req, res, function multerUpload(err) {
            console.log("req inside upload------", req)
            if (err) {
                console.log(err)
            } else {
                if (req.files) {
                    multipleFile(req).then(element => {
                        console.log("body-----------", req.body);
                        console.log("element--------", element);
                        req.body.images = element;
                        console.log("req.body-----------", req.body)
                        var dishSchemaData = new dishSchema(req.body);
                        console.log("Dish------------", dishSchemaData);
                        dishSchemaData.save(function (err) {
                            if (err) throw err;
                        })
                            .then(item => {
                                res.send("Name saved to database");
                            })
                            .catch(err => {
                                res.status(400).send("Unable to save to database");
                            });
                        //res.send("uploaded successfully")
                    }).catch(err => {
                        console.log("error---------", err)
                    })
                } else {
                    console.log("inside else--------")
                }
            }
        })
    });

    app.post("/addDishReview", (req, res, next) => {
        console.log("req----------", req.files);
        console.log(req.body);
        var user_id, review_id, reviewSaved, review_rating, average_rating, first_name, last_name;
        console.log('headers---------', req.headers)
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            else {
                console.log('decoded-----------', decoded);
                user_id = decoded._id;
                first_name = decoded.first_name;
                last_name = decoded.last_name;
                req.user_id = decoded._id;
            }
        });
        upload(req, res, function multerUpload(err) {
            //console.log("req inside upload------", req)
            if (err) {
                console.log(err)
                res.send("issue with saving");
            } else {
                req.s3Path = user_id + '/' + req.body.mapping_id;
                multipleFile(req)
                    .then(element => {
                        console.log("body-----------", req.body);
                         async.series([
                            function saveReview(callback) {
                                //using findByIdAndUpdate
                                var newId;
                                if (req.body.alreadyReviewedId !== 'null') {
                                    newId = req.body.alreadyReviewedId
                                } else {
                                    newId = new ObjectId();
                                }
                                reviewSchema.findByIdAndUpdate(
                                    {
                                        _id: newId
                                    },
                                    {
                                        rating : req.body.rating,
                                        review : req.body.review,
                                        images : element,
                                        "user.user_id" : new ObjectId(user_id),
                                        "user.first_name" : first_name,
                                        "user.last_name" : last_name,
                                    },
                                    {
                                        upsert: true,
                                        new: true
                                    },
                                    function (err, review) {
                                        if (err) {
                                            console.log("error in storing resto item data 1-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data------- 1", review);
                                            console.log("review id------", review._id);
                                            review_id = review.id;
                                            console.log("review_id---------", review_id);
                                            review_rating = review.rating;
                                            reviewSaved = review
                                            callback(null, review);
                                        }
                                    }
                                )
                            },

                            function pullReviews(callback) {
                                dishRestaurantMappingSchema.findOneAndUpdate(
                                    {
                                        _id: req.body.mapping_id
                                    },
                                    {
                                       $pull: {
                                           review_id: {
                                               _id: review_id,
                                           },
                                           reviews: { _id: review_id}
                                       }
                                    },
                                    
                                    function (err, result) {
                                        if (err) {
                                            console.log("error in storing resto item data 1-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data------- 1", result);
                                            callback(null, result);
                                        }
                                    }
                                )
                            },

                            function addRecommendValueInDishRestoMapping(callback) {
                                dishRestaurantMappingSchema.findOneAndUpdate(
                                    {
                                        _id: req.body.mapping_id
                                    },
                                    {
                                        $push: {
                                            review_id: {
                                                _id: review_id, 
                                                rating: review_rating
                                            },
                                            reviews: { $each: [reviewSaved], $sort: { date: -1 }, $slice: 3 }
                                        },
                                        $inc: {
                                            review_counts: 1
                                        }
                                    },
                                    {
                                        upsert: true,
                                        new: true
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log("error in storing resto item data 1-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data------- 1", result);
                                            callback(null, result);
                                        }
                                    }
                                )
                            },

                            function calculateAverage(callback) {
                                // To calculate average rating using aggregate of mongo
                                dishRestaurantMappingSchema.aggregate([
                                    {
                                        $match: {
                                            _id: ObjectId(req.body.mapping_id)
                                        }
                                    },
                                    {
                                        $project: {
                                            average_rating: {$avg: "$review_id.rating"}
                                        }
                                    }
                                    
                                ], function (err, result) {
                                    if (err) {
                                        console.log("error in storing average rating-------", err);
                                        callback(err);
                                    } else {
                                        console.log("average rating----------", result);
                                        average_rating = result[0].average_rating;
                                        callback(null, result);
                                    }
                                })
                            },

                            function saveAverageRating(callback) {
                                // To calculate average rating using aggregate of mongo
                                dishRestaurantMappingSchema.findOneAndUpdate(
                                    {
                                        //"restaurant_id": req.body.restaurant_id,
                                        _id: req.body.mapping_id
                                    },
                                    {
                                        average_rating: average_rating
                                    },
                                    {
                                        upsert: true,
                                        new: true
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log("error in storing resto item data 1-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data average rating", result);
                                            callback(null, result);
                                        }
                                    }
                                )
                            }
                        ],
                            function increaseCountOfUserReview(err, results) {
                                if (err) return res.status(400).send({ message: "Please try in some time", error: err });
                                userSchema.findOneAndUpdate(
                                    {
                                        _id: user_id
                                    },
                                    {
                                        $inc: {
                                            no_of_reviews: 1
                                        }
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log("error in storing resto item data 2--------", err);
                                            return res.status(400).send({ message: "Please try in some time", error: err })
                                        } else {
                                            console.log("stored data------- 2", result);
                                            if (result === null) {
                                                return res.status(400).send({ message: "Please try in some time", error: "Not incremented for user." })
                                            }
                                            return res.status(200).send({ message: "successful", success: results[3] })
                                        }
                                    }
                                ) 
                            })
                    }).catch(err => {
                        console.log("error---------", err);
                        res.status(400).send({ message: "Please try in some time", error: err })
                    })
            }
        })
    });

    app.post("/addRestaurantReview", (req, res, next) => {
        console.log("req----------", req.files);
        console.log(req.body);
        upload(req, res, function multerUpload(err) {
            //console.log("req inside upload------", req)
            if (err) {
                console.log(err)
                res.send("issue with saving");
            } else {
                var user_id, review_id, reviewSaved, review_rating, average_rating, first_name, last_name;
                console.log('headers---------', req.headers)
                var token = req.headers['x-access-token'];
                if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

                jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
                    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
                    else {
                        console.log('decoded-----------', decoded);
                        user_id = decoded._id;
                        first_name = decoded.first_name;
                        last_name = decoded.last_name;
                    }
                });
                req.s3Path = user_id + '/' + req.body.restaurant_id;
                multipleFile(req)
                    .then(element => {
                        console.log("body-----------", req.body);
                        async.series([
                            function saveReview(callback) {
                                //using findByIdAndUpdate
                                var newId;
                                if (req.body.alreadyReviewedId !== 'null') {
                                    newId = new ObjectId(req.body.alreadyReviewedId)
                                } else {
                                    newId = new ObjectId();
                                }
                                reviewSchema.findByIdAndUpdate(
                                    {
                                        _id: newId
                                    },
                                    {
                                        rating: req.body.rating,
                                        recommended: req.body.rating === "5" ? true : false,
                                        review: req.body.review,
                                        images: element,
                                        "user.user_id": new ObjectId(user_id),
                                        "user.first_name": first_name,
                                        "user.last_name": last_name,
                                    },
                                    {
                                        upsert: true,
                                        new: true
                                    },
                                    function (err, review) {
                                        if (err) {
                                            console.log("error in storing resto item data 1-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data------- 1", review);
                                            console.log("review id------", review._id);
                                            review_id = review.id;
                                            console.log("review_id---------", review_id);
                                            review_rating = review.rating;
                                            reviewSaved = review
                                            callback(null, review);
                                        }
                                    }
                                )
                            },
                            function pullReviews(callback) {
                                restaurantSchema.findOneAndUpdate(
                                    {
                                        _id: req.body.restaurant_id
                                    },
                                    {
                                        $pull: {
                                            review_id: {
                                                _id: review_id,
                                            },
                                            reviews: { _id: review_id }
                                        }
                                    },

                                    function (err, result) {
                                        if (err) {
                                            console.log("error in pull-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data------- 1", result);
                                            callback(null, result);
                                        }
                                    }
                                )
                            },

                            function addRecommendValueInDishRestoMapping(callback) {
                                restaurantSchema.findOneAndUpdate(
                                    {
                                        //"restaurant_id": req.body.restaurant_id,
                                        _id: req.body.restaurant_id
                                    },
                                    {
                                        $push: {
                                            review_id: {
                                                _id: review_id,
                                                rating: review_rating
                                            },
                                            reviews: { $each: [reviewSaved], $sort: { date: -1 }, $slice: 3 }
                                        },
                                        $inc: {
                                            review_counts: 1
                                        }
                                    },
                                    {
                                        upsert: true,
                                        new: true
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log("error in storing resto item data 1-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data------- 1", result);
                                            callback(null, result);
                                        }
                                    }
                                )
                            },
                            function calculateAverage(callback) {
                                // To calculate average rating using aggregate of mongo
                                restaurantSchema.aggregate([
                                    {
                                        $match: {
                                            _id: ObjectId(req.body.restaurant_id)
                                        }
                                    },
                                    {
                                        $project: {
                                            average_rating: { $avg: "$review_id.rating" }
                                        }
                                    }

                                ], function (err, result) {
                                    if (err) {
                                        console.log("error in storing average rating-------", err);
                                        callback(err);
                                    } else {
                                        console.log("average rating----------", result);
                                        average_rating = result[0].average_rating;
                                        callback(null, result);
                                    }
                                })
                            },

                            function saveAverageRating(callback) {
                                // To calculate average rating using aggregate of mongo
                                restaurantSchema.findOneAndUpdate(
                                    {
                                        //"restaurant_id": req.body.restaurant_id,
                                        _id: req.body.restaurant_id
                                    },
                                    {
                                        average_rating: average_rating
                                    },
                                    {
                                        upsert: true,
                                        new: true
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log("error in storing resto item data 1-------", err);
                                            callback(err);
                                        } else {
                                            console.log("stored data average rating", result);
                                            callback(null, result);
                                        }
                                    }
                                )
                            }
                        ],
                            function increaseCountOfUserReview(err, results) {
                                if (err) throw err;
                                userSchema.findOneAndUpdate(
                                    {
                                        _id: user_id
                                    },
                                    {
                                        $inc: {
                                            no_of_reviews: 1
                                        }
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log("error in storing resto item data 2--------", err);
                                            return res.status(400).send({ message: "Please try in some time", error: err })
                                            //callback(err);
                                        } else {
                                            console.log("stored data------- 2", result);
                                            //callback(result);
                                            if (result === null) {
                                                return res.status(400).send({ message: "Please try in some time", error: "Not incremented for user." })
                                            }
                                            return res.status(200).send({ message: "successful", success: results[1] })
                                            //res.send(results[1]);
                                        }
                                    }
                                )
                            })
                    }).catch(err => {
                        console.log("error---------", err);
                        res.status(400).send({ "message": "Please try in some time", error: err })
                    })
            }
        })
    });

    app.get('/findReviews', function (req, res) {
        dishRestaurantMappingSchema.find({ _id: "5af6fd3ece31f73679fce2c3" })
            .populate('dish_id')
            .exec(function (err, story) {
                if (err) return handleError(err);
                console.log(story);
                res.send(story);
                // prints "The author is Ian Fleming"
            });
    })


    app.get('/uploadimage', function (req, res) {
        console.log("jhihhi");
        res.render('uploadImages.ejs');
        //res.sendFile(__dirname + '/addResto.html');
    });

    var imageUrl = [];

    app.post('/addProfileImage', function (req, res) {
        upload(req, res, function multerUpload(err) {
            //console.log("req inside upload------", req)
            if (err) {
                console.log(err)
                res.send("issue with saving");
            } else {
                var user_id, review_id, reviewSaved, review_rating, average_rating, first_name, last_name;
                console.log('headers---------', req.headers)
                var token = req.headers['x-access-token'];
                if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

                jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
                    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
                    else {
                        console.log('decoded-----------', decoded);
                        user_id = decoded._id;
                        first_name = decoded.first_name;
                        last_name = decoded.last_name;
                    }
                });
                req.s3Path = user_id + '/' + 'profile_pic';
                multipleFile(req)
                .then(element => {
                    userSchema.update(
                        {
                            _id: user_id
                        },
                        {
                            image: element
                        },
                        function (err, user) {
                            if(err) {
                                console.log("error-------", err);
                                res.status(400).send({message: "Unsuccessfull in updating the image", error: err})
                            } else {
                                res.status(200).send({message: "Successfully updated profile pic", success: user})
                            }
                        }
                    )
                })
                .catch(err => {
                    console.log("error-------", err);
                    res.status(400).send({ message: "Unsuccessfull in updating the image", error: err })
                })
            }
        })
    })

    app.post('/uploadimage', function (req, res) {
        console.log("Photos------",req.files);
        console.log(req.body);
        fs.readFile(req.body.Image, function (err, data) {
            if (err) { throw err; }
            const s3Params = {
                Bucket: S3_BUCKET,
                Key: "photoActual",
                Expires: 60,
                Body: data,
                ContentType: "mimeType",
                ACL: 'public-read'
            };

            s3.upload(s3Params, function (error, data) {
                if (error) {
                    winston.log("Error uploading data: ", error);
                    console.log("Error uploading data: ", error);
                    const returnData = {
                        signedRequest: data,
                        url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`
                    };
                    res.write(JSON.stringify(returnData));
                    reject(error);
                } else {
                    console.log("response--------------", data);
                    winston.log("Successfully uploaded data to myBucket/myKey");
                    console.log("Successfully uploaded data to myBucket/myKey");
                    //imageUrl[i] = data.Location;
                    console.log(imageUrl, "imageUrl------")
                    res.send("Image saved to database");
                }
                //callback();
            });
        })
    });

    multipleFile = function (req) {
        console.log("req", req)
        return new Promise(function (resolve, reject) {
            console.log("req.files---------", req.files)
            async.forEachOf(req.files, function (element, i, callback) {
                console.log("inside here---------", element)
                var data = element.buffer;
                const fileName = element.originalname;
                const mimeType = element.mimeType;

                const s3Params = {
                    Bucket: S3_BUCKET,
                    Key: req.s3Path + '/' + fileName,
                    Expires: 60,
                    Body: data,
                    ContentType: mimeType,
                    ACL: 'public-read'
                };

                s3.upload(s3Params, function (error, data) {
                    if (error) {
                        winston.log("Error uploading data: ", error);
                        console.log("Error uploading data: ", error);
                        const returnData = {
                            signedRequest: data,
                            url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`
                        };
                        res.write(JSON.stringify(returnData));
                        reject(error);
                    } else {
                        console.log("response--------------", data);
                        winston.log("Successfully uploaded data to myBucket/myKey");
                        console.log("Successfully uploaded data to myBucket/myKey");
                        imageUrl[i] = data.Location;
                        console.log(imageUrl, "imageUrl------")
                        //res.send("Image saved to database");
                    }
                    callback();
                });
            }, function (err) {
                if (err) {
                    reject(err)
                }
                else {
                    console.log("inside callback--------")
                    resolve(imageUrl);
                }
            });
        });
    }

    app.get("/searchRestoName", (req, res) => {
        console.log("searching");
        restaurantSchema.find({}, { restaurant_name: 1 },
            function (err, resto) {
                if (err) return console.error(err);
                res.send(JSON.stringify({ "results": resto }));
            }
        );
    });

    app.get("/getRestaurants", (req, res) => {
        restaurantSchema.find(function (err, results) {
            if (err) {
                console.log("error in storing resto item data 2--------", err);
                res.status(400).send({ message: "Please try in some time", error: err })
            } else {
                console.log("stored data------- 2", results);
                res.status(200).send({ message: "successful", success: results })
            }
        });
    });

    app.get("/getReviews", (req, res) => {
        reviewSchema.find(function (err, resto) {
            res.json({ docs: resto })
        });
    });

    app.get("/getDishes", (req, res) => {
        dishSchema.find(function (err, users) {
            res.json({ docs: users })
        });
    });

    app.get("/getSelectedRestaurant", (req, res) => {
        restaurantSchema.find({ "_id": req.query.id }, function (err, restaurant) {
            if (err) {
                res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
            } else {
                res.status(200).send({ message: "Here are top 10 dishes", success: restaurant });
            }
        })
    });

    app.post("/addDishImageByUser", (req, res, next) => {
        console.log('headers---------', req);
        console.log("req.body----", req.body);
        console.log("req.files---------", req.files)

        var user_id, review_id, reviewSaved, review_rating, average_rating, first_name, last_name;
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.', error: err });
            else {
                console.log('decoded-----------', decoded);
                user_id = decoded._id;
                first_name = decoded.first_name;
                last_name = decoded.last_name;
            }
        });
        upload(req, res, function multerUpload(err) {
            //console.log("req inside upload------", req)
            if (err) {
                console.log(err)
                res.send("issue with saving");
            } else {
                req.s3Path = req.body.date + '/' + user_id + '/' + req.body.mapping_id;
                multipleFile(req)
                    .then(element => {
                        console.log("body-----------", req.body);
                        dishRestaurantMappingSchema.findByIdAndUpdate(
                            req.body.mapping_id, 
                            {
                                $push: {
                                    "images.byUser": {
                                        user_id: user_id, 
                                        first_name: first_name,
                                        last_name: last_name,
                                        imageUrl: element[0]
                                    }
                                }
                            },
                            {
                                new: true
                            },
                            function (err, dish) {
                                if(err) {
                                    console.log("error----------", err);
                                } else {
                                    console.log("dish updated-----", dish);
                                    userSchema.findByIdAndUpdate(
                                        user_id, 
                                        {
                                            $push: {
                                                dish_images_uploads: {
                                                    restaurant_name: dish.restaurant_name,
                                                    dish_name: dish.dish_name,
                                                    locality: dish.locality,
                                                    mapping_id: dish._id,
                                                    imageUrl: element[0]
                                                }
                                            }
                                        },
                                        {
                                            new: true
                                        },
                                        function (err, user) {
                                            if(err) {
                                                console.log("error-----------", err)
                                            } else {
                                                console.log("user--------", user);
                                                res.status(200).send({ message: "uploaded successfully", success: dish })
                                            }
                                        }
                                    )
                                }
                            }
                        )
                    }).catch(err => {
                        console.log("error---------", err);
                        res.status(400).send({ message: "Please try in some time", error: err })
                    })
            }
        })
    });

}
