// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    first_name: {type: String, default: ''},
    last_name: { type: String, default: '' },
    email: String,
    password: String,
    recommended_dish_mappings: [{ type: mongoose.Schema.Types.ObjectId, ref: 'dish_restaurant_mappings' }],
    recommended_restaurants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'restaurants' }],
    no_of_recommendations: {type: Number, default: 0},
    no_of_reviews: { type: Number, default: 0 },
    image: String,
    foodie_level: { type: Number, default: 1 },
    phone_number: String,
    address: {
        building: String,
        latitude: Number,
        longitude: Number,
        street: String,
        locality: String,
        city: String,
        zipcode: String,
        state: String,
        country: String
    },
    dish_images_uploads: [], // Object {restaurant name, dish name, restaurant locality, imageUrl, id}
    facebook_id: String,
    facebook_token: String,
    google_id: String,
    google_token: String,

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('users', userSchema);