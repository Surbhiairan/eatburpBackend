var users = require('../../app/models/users');
var reviewSchema = require('../models/reviewSchema');
var restaurantSchema = require('../models/restaurantSchema');
var dishRestaurantMappingSchema = require('../models/dishRestaurantMappingSchema');
var bodyParser = require('body-parser');
var Promise = require('promise');
var express = require('express');
var bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
var async = require('async');

var app = express();

app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
    extended: true
}));

module.exports = function usersAPI(app) {

    app.post("/signUp", (req, res) => {
        users.findOne({ email: req.body.email }, function (err, user) {
            if (err) throw err;
            if (user) {
                res.status(400).json({
                    error: "User already registered. Please login."
                })
            } else {
                var newUser = new users(req.body);
                newUser.first_name = req.body.first_name;
                newUser.last_name = req.body.last_name;
                newUser.image = '';
                newUser.phone_number = '';
                newUser.address = {
                    building: '',
                    latitude: 0,
                    longitude: 0,
                    street: '',
                    locality: '',
                    city: '',
                    zipcode: '',
                    state: '',
                    country: ''
                }
                console.log('newUser-----------', newUser);
                newUser.password = newUser.generateHash(req.body.password);
                newUser.save(function (err) {
                    if (err) throw err;
                })
                .then(item => {
                    console.log('item--------', item)
                    const JWTToken = jwt.sign({
                        _id: item._id,
                        first_name: item.first_name,
                        last_name: item.last_name
                    },
                        process.env.JWT_SECRET,
                    );
                    res.status(200).json({
                        success: 'Welcome to the JWT Auth',
                        token: JWTToken,
                        first_name: item.first_name,
                        last_name: item.last_name
                    });
                })
                .catch(err => {
                    res.status(400).json({ error: "Unable to sign up." });
                });
            }
        })

    });

    app.post("/signIn", (req, res) => {
        console.log(req.body);
        users.find({ email: req.body.email }, function (err, user) {
            console.log(user);
            if (err) {
                return res.send(err)
            } else if (user.length === 0) {
                console.log("no user")
               return res.status(401).json({
                    error: 'Please register as new user'
                });
            } else if (user[0] && !validPassword(req.body.password, user[0].password)) {
                return res.status(401).json({
                    error: 'Please enter correct password'
                });
            } else {
                console.log('user-------', user[0]._id)
                const JWTToken = jwt.sign({
                    _id: user[0]._id,
                    first_name: user[0].first_name,
                    last_name: user[0].last_name
                },
                    process.env.JWT_SECRET,
                );
                console.log(JWTToken);
                res.status(200).json({
                    success: 'Welcome to the JWT Auth',
                    token: JWTToken,
                });
            }
        })
    });

    app.get("/searchUsers", (req, res) => {
        console.log(req.query.user);
        var user_id;
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            else {
                console.log('decoded-----------', decoded);
                user_id = decoded._id
            }
        });

        users.find(
            { 
                "first_name": 
                    { "$regex": req.query.firstName, "$options": "i" },
                "last_name": 
                    { "$regex": req.query.lastName, "$options": "i" }
            }, 
            function (err, results) {
                if (err) {
                    res.status(400).send({ message: "Please try in some time", error: err })
                } else {
                    console.log("stored data------- 2", results);
                    res.status(200).send({ message: "successful", success: results })
                }
            }
        )
    })

   /*  app.get("/getUserDetails", (req, res) => {
        var user_id;
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            else {
                console.log('decoded-----------', decoded);
                user_id = decoded._id
            }
        });
        
        users.findById(user_id, function (err, results) {
            if (err) {
                console.log("error in storing resto item data 2--------", err);
                res.status(400).send({ message: "Please try in some time", error: err })
            } else {
                console.log("stored data------- 2", results);
                res.status(200).send({ message: "successful", success: results })
            }
        })
    }) */

    var validPassword = function (password1, password) {
        //console.log("here------------", password, password1)
        var value = bcrypt.compareSync(password1, password);
        console.log(value);
        return value;
    };

    //not of use as user details got this covered
    app.get("/getUserReviews", (req, res) => {
        console.log("req.header-------", req.header);
        var user_id;
        var token = req.headers['x-access-token'];
        console.log(token);
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            else {
                console.log('decoded-----------', decoded);
                user_id = decoded._id;
            }
        });

        reviewSchema.find({"user.user_id": user_id}, function name(err, reviews) {
            if (err) {
               return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
            } else {
                //res.status(200).send({ message: "All good", success: reviews });
                let userReviews = [];
                async.forEachOf(reviews, function (review, index, callback) {
                    console.log("review_id---------", review._id)
                    dishRestaurantMappingSchema.find({ "review_id._id": review.id }, function getDishDetail(err, dish) {
                        if (err) {
                            return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                        } else {
                            console.log("review_id---------", review._id)
                            console.log("dish detail----------", dish);
                            if (dish.length === 0) {
                                restaurantSchema.find({ "review_id._id": review.id }, function getRestaurantDetail(err, restaurant) {
                                    if (err) {
                                        return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                                    } else {
                                        console.log("review_id---------", review._id)
                                        console.log("restaurant detail----------", restaurant);
                                        if (restaurant.length === 0) {
                                            return res.status(500).send({
                                                message: 'Please wait for some time and try again.',
                                                error: "Review not found in dish. Please check with backend"
                                            })
                                        }
                                        userReviews[index] = {
                                            review: review,
                                            restaurant_detail: restaurant[0]
                                        }
                                    }
                                    callback(null, userReviews);
                                })
                            } else {
                                userReviews[index] = {
                                    review: review,
                                    dish_detail: dish[0]
                                }
                                callback(null, userReviews);
                            }
                        }
                       
                    })
                }, function callbackFunction(err, result) {
                    if(err) {
                        return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                    }
                    console.log("over here")
                    return res.status(200).send({ message: "All good", success: userReviews });
                })
            }
        })
    });

    //not of use as user detail got this covered.
    app.get("/getUserRecommendation", (req, res) => {
        console.log("req.header-------", req.header);
        var user_id;
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            else {
                console.log('decoded-----------', decoded);
                user_id = decoded._id
            }
        });

        users.findById(user_id, function name(err, user) {
            if (err) {
                return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
            } else {
                let userReviews = [];
                var i = 0;
                async.forEachOf(user.recommended_dish_mappings, function (dish_id, index, callback) {
                    console.log("dish id", dish_id);
                    dishRestaurantMappingSchema.findById(dish_id, function getDishDetail(err, dish) {
                        if (err) {
                            return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                        } else {
                            console.log("dish details", dish);
                            userReviews[index] = dish;
                            i= index;
                        }
                        callback(null, userReviews);
                    })
                }, function callbackFunction(err, result) {
                    if (err) {
                        return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                    }
                    console.log("over here dish")
                    console.log("results------", result)
                    async.forEachOf(user.recommended_restaurants, function (restaurant_id, index, callback) {
                        console.log("dish id", restaurant_id);
                        restaurantSchema.findById(restaurant_id, function getDishDetail(err, restaurant) {
                            if (err) {
                                return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                            } else {
                                console.log("restaurant----", restaurant);
                                userReviews[i + 1] = restaurant;
                            }
                            callback(null, userReviews);
                        })
                    }, function callbackFunction(err, result) {
                        if (err) {
                            return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                        }
                        console.log("over here")
                        res.status(200).send({ message: "All good", success: userReviews });
                    })
                })
                
            }
        })
    })

    app.get("/getUserDetails", (req, res) => {
        console.log("req.header-------", req.header);
        var user_id;
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            else {
                console.log('decoded-----------', decoded);
                user_id = req.query.user_id || decoded._id
            }
        });
        var recommended_dish_ids, 
            recommended_restaurants_ids, 
            userReviewsIds = [];
        var userRestaurantReview = [];
        var i = 0;
        var userRecommendations = [];
        var userReviews = [];
        async.series([
            function getUser(callback) {
                users.findById(user_id, function (err, result) {
                    if (err) {
                        console.log("error in storing resto item data 1-------", err);
                        callback(err);
                    } else {
                        console.log("stored data------- 1", result);
                        recommended_dish_ids = result.recommended_dish_mappings;
                        recommended_restaurants_ids = result.recommended_restaurants;
                        callback(null, result);
                    }
                })
            },  //get all recommendations for dishes
            function getRecommendedDish(callback) {
                async.forEachOf(recommended_dish_ids, function (dish_id, index, callback) {
                    console.log("dish id", dish_id);
                    dishRestaurantMappingSchema.findById(dish_id, function getDishDetail(err, dish) {
                        if (err) {
                            //return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                            callback(err);
                        } else {
                            console.log("dish details", dish);
                            userRecommendations[index] = dish;
                            i++;
                        }
                        callback();
                    })
                }, function callbackFunction(err) {
                    callback(err, userRecommendations);
                })
            },
            function getRecommendedRestaurant(callback) {
                async.forEachOf(recommended_restaurants_ids, function (restaurant_id, index, callback) {
                    console.log("restaurant_id", restaurant_id);
                    restaurantSchema.findById(restaurant_id, function getDishDetail(err, restaurant) {
                        if (err) {
                            //return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                            callback(err);
                        } else {
                            console.log("restaurant----", restaurant);
                            userRecommendations[i] = restaurant;
                            i++
                        }
                        callback();
                    })
                }, function callbackFunction(err) {
                    callback(err, userRecommendations);
                })
            },
            function getReviewIds(callback) {
                reviewSchema.find({ "user.user_id": user_id }, function name(err, reviews) {
                    if (err) {
                        callback(err)
                        //return res.status(500).send({ message: 'Please wait for some time and try again.', error: err })
                    } else {
                        userReviewsIds = reviews
                        callback(null, reviews);
                    }
                })
            },
            function getDishReviews(callback) {
                var k = 0;
                var j = 0;
                async.forEachOf(userReviewsIds, function (review, index, callback) {
                    dishRestaurantMappingSchema.find({ "review_id._id": review.id }, function (err, dish) {
                        if (err) {
                            callback(err)
                        } else {
                            console.log("review_id---------", review._id)
                            console.log("dish detail----------", dish);
                            if (dish.length !== 0) {
                                userReviews[j] = {
                                    review: review,
                                    dish_detail: dish[0]
                                }
                                j++;
                                //userReviewsIds.shift();
                            } else {
                                userRestaurantReview[k] = review;
                                k++;
                            }
                        }
                        callback();
                    })
                }, function (err) {
                    callback(err, userReviews);
                })
                
            },
            function getRestaurantReviews(callback) {
                var index = userReviews.length;
                var j =0;
                async.forEachOf(userRestaurantReview, function (review, i, callback) {
                    restaurantSchema.find({"review_id._id": review.id}, function (err, restaurant) {
                        if (err) {
                            callback(err)
                        } else {
                            console.log("review_id---------", review._id)
                            console.log("restaurant detail----------", restaurant);
                            if (restaurant.length !== 0) {
                                userReviews[index + j] = {
                                    review: review,
                                    restaurant_detail: restaurant[0]
                                }
                                j++;
                                //userReviewsIds.shift();
                            } else {
                                callback("Please check the database");
                            }
                        }
                        callback();
                    })
                }, function (err) {
                    callback(err, userReviews);
                })
            }
        ],
        function lastFunction(err, results) {
            if (err) throw err;
            console.log("final result--------", results);
            var userInfo = {};
            userInfo.user = results[0];
            userInfo.recommendations = results[2];
            userInfo.reviews = results[5];

            res.status(200).send({ message: "User information", success: userInfo })
        })
    })
}
