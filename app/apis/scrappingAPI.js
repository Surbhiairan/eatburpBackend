var bodyParser = require('body-parser');
var Promise = require('promise');
var express = require('express');
const jwt = require('jsonwebtoken');
var ObjectId = require('mongoose').Types.ObjectId;
var fs = require('fs');
var app = express();
var async = require('async');

var dishSchema = require('../../app/models/dishSchema');
var dishRestaurantMappingSchema = require('../../app/models/dishRestaurantMappingSchema');
var restaurantSchema = require('../models/restaurantSchema');

app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
    extended: true
}));



module.exports = function dishRestaurantMappingAPI(app, AWS) {

    var s3 = new AWS.S3();
    const S3_BUCKET = process.env.S3_BUCKET;

    app.post('/uploadScrappedImage', function (req, res) {
        console.log("Photos------",req.files);
        console.log(req.body);
        var restaurant_id, restaurant_name;
        
        async.forEach(req.body.data, function name(params, callback) {
           console.log("params-----", params)
            var restaurant = {
                restaurant_name: params.restaurant.name,
                address: {
                    building: params.restaurant.address,
                    latitude: params.restaurant.geo_location[0],   //index
                    longitude: params.restaurant.geo_location[1],  //index
                    city: 'Indore',
                    state: 'Madhya Pradesh',
                    country: 'India'
                },
                phone_number: params.restaurant.contact,
                average_cost_for_two: params.restaurant.price_two,

                famous_dishes: params.restaurant.what_people_love_here,

                average_rating: 0, //index
                cuisines: params.restaurant.cuisines
            }
            async.series([
                function insertRestaurant(callback) {
                    console.log("params", params.dish_mapping)
                    restaurantSchema.create(restaurant, function name(err, restaurantInfo) {
                        if (err) {
                            console.log(err);
                            callback(err);  
                            //res.status(400).send({ message: "Please try again after sometime", error: err })
                        } else {
                            console.log("restaurant info--------", restaurantInfo);
                            restaurant_id = restaurantInfo._id;
                            restaurant_name = restaurantInfo.restaurant_name
                            callback(null, restaurantInfo)
                        }
                    })
                },
                function insertDishes(callback2) {
                    console.log("inside here---");
                    console.log(params.dish_mappings)
                    async.forEachOf(params.dish_mappings, function singleMapping(dish, index, callback3) {
                        console.log("index-------", index);
                        async.series([
                            function imageSavingToS3(callback4) {
                                imageSaving(dish)
                                    .then(s3Data => {
                                        console.log("s3data------", s3Data)
                                        callback4(null, s3Data);
                                    })
                                    .catch(err => {
                                        callback4(err);
                                    })
                            },
                            function insertDish(callback5) {
                                dishSchema.findOneAndUpdate(
                                    {
                                        "dish_name": dish.dish_name
                                    },
                                    {
                                        $setOnInsert: {
                                            "dish_name": dish.dish_name,
                                            "type": dish.type
                                        }
                                    },
                                    {
                                        upsert: true,
                                        new: true
                                    },
                                    function (err, result) {
                                        if (err) {
                                            callback5(err);
                                        } else {
                                            callback5(null, result)
                                        }
                                    }
                                )
                            }
                        ], function insertIntoMenu(err, results) {
                            var price = Number(dish.dish_price);
                            console.log("price---------", price);
                            console.log("results----------", results)
                            dishRestaurantMappingSchema.findOneAndUpdate(
                                {
                                    "restaurant_id": restaurant_id,
                                    "dish_id": results[1]._id
                                },
                                {
                                    $setOnInsert: {
                                        "restaurant_id": restaurant_id,
                                        "dish_name": results[1].dish_name,
                                        "restaurant_name": restaurant_name,
                                        "dish_id": results[1]._id,
                                        "price": price,
                                        "dish_category": dish.dish_category,
                                        "review_id": [],
                                        "average_rating": 0,
                                        "images": {
                                            byAdmin: [results[0]]
                                        },
                                        "recommended": 0,
                                        "reviews": [],
                                        "review_counts": 0,
                                        "experimental_dish": false
                                    }
                                },
                                {
                                    upsert: true,
                                    new: true
                                },
                                function (err, result) {
                                    if (err) {
                                        console.log(err);

                                    } else {
                                        console.log(result);
                                    }
                                    callback3(null, results);
                                }
                            )
                        })
                    }, function foreachCallback(err, results) {
                        if(err) {
                            callback2(err);
                        }
                        else{
                            callback2(null, results);
                        }
                    })
                }
            ], function restaurantCallback(err, results) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, results);
                }
            })
        }, function finalCallback(err, results) {
            if (err) {
                res.status(400).send({message: "Something is wrong", error: err})
            } else {
                res.status(200).send({message: "All good", success: results})
            }
        })
    });

    imageSaving = (body) => {
        return new Promise(function (resolve, reject) {
            if(body.image) {
                fs.readFile(body.image, function (err, data) {
                    if (err) { throw err; }
                    const s3Params = {
                        Bucket: S3_BUCKET,
                        Key: body.dish_name,
                        Expires: 60,
                        Body: data,
                        ContentType: "mimeType",
                        ACL: 'public-read'
                    };
                    s3.upload(s3Params, function (error, data) {
                        if (error) {
                            //winston.log("Error uploading data: ", error);
                            console.log("Error uploading data: ", error);
                            const returnData = {
                                signedRequest: data,
                                url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`
                            };
                            res.write(JSON.stringify(returnData));
                            reject(error);
                        } else {
                            console.log("response--------------", data);
                            console.log("Successfully uploaded data to myBucket/myKey");
                            resolve(data.Location);
                        }
                    });
                })
            } else {
                resolve("");
            }
            
        })
    }
}